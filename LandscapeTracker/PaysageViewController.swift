//
//  PaysageViewController.swift
//  LandscapeTracker
//
//  Created by Xenon on 28/09/2017.
//  Copyright © 2017 ABCDEFAB. All rights reserved.
//

import UIKit
import Cosmos
import MapKit
import CoreLocation


/// Classe pilotant l'ecran de detail/modification/creation de paysage
class PaysageViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, MKMapViewDelegate {
    
    //Le paysage en cours d'affichage
    var paysageEnCours:Paysage?
    
    //L'instance du paysageManager
    let paysageManager = PaysageManager.shared
    
    //L'instance Realm qui va gerer la persistence de nos données
    var realm = (UIApplication.shared.delegate as! AppDelegate).realm
    
    //Manager permettant la gestion de la localisation de l'utilisateur
    var locationManager = CLLocationManager()

    @IBOutlet weak var ui_textField_nom: UITextField!
    @IBOutlet weak var ui_imageView: UIImageView!
    @IBOutlet weak var ui_cosmosView: CosmosView!
    @IBOutlet weak var ui_mapView: MKMapView!
    @IBOutlet weak var ui_btn_share: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //On prévient le composant mapView que cette classe est son delegate (conforme au protocole MKMapViewDelegate)
        ui_mapView.delegate = self
        
        //Si on entre dans le IF, nous sommes en modification/consultation
        if let paysage = paysageEnCours {
            ui_textField_nom.text = paysage.nom
            ui_cosmosView.rating = Double(paysage.note)
            ui_imageView.image = paysage.photo?.toUIImage()
            
            //Map
            //En cas de modification/consultation, on veut créer une épingle à partir des données de localisation
            //que l'utilisateur a saisi lors de la création
            let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2D(latitude: paysage.latitude, longitude: paysage.longitude)
            annotation.title = "Lieu"
            //On ajoute l'épingle à la liste des objets à positionner du composant MapView.
            ui_mapView.addAnnotation(annotation)
            //Dans ce mode, on ne veut plus montrer la localisation de l'utilisateur
            ui_mapView.showsUserLocation = false
        } else {
            //En mode création, on veut connaitre la localisation de l'utilisateur
            if(CLLocationManager.authorizationStatus() == .notDetermined) {
                locationManager.requestWhenInUseAuthorization()
            }
            
            //En creation on ne veut pas proposer le partage car le paysage peut ne pas etre complet
            //(On pourrait le maquer entierement, mais il faut faire une autre ligne de code et j'ai la flemme
            ui_btn_share.isEnabled = false
        }
    }
    
    
    /// Méthode appelée par le framework juste après l'apparition de la vue à l'ecran
    ///
    /// - Parameter animated: <#animated description#>
    override func viewDidAppear(_ animated: Bool) {
        //En modification/consultation, on veut zoomer autour de la position de l'epingle situant le lieu de la prise de photo
        if let paysage = paysageEnCours {
            //Creation d'une region avec les coordonnées du paysage, et un delta de 0.2
            let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: paysage.latitude, longitude: paysage.longitude), span: MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2))
            //On set la region en précisant qu'on veut une animation
            ui_mapView.setRegion(region, animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /// Méthode appelée par le bouton "Annuler" de l'ecran, permettant de revenir à l'ecran précédent
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func doAnnuler(_ sender: Any) {
        //Si le presentingVC est un UINavigationController, c'est que nous sommes dans le cas de l'ajout de paysage
        if presentingViewController is UINavigationController {
            //Ce controller etant modal, il n'est pas ajouté au NavigationController de base, il faut donc enlever entièrement le controller via un dismiss
            dismiss(animated: true)
        } else {
            //Sinon, il s'agit d'une page ajouté dans la pile du navigationController de base, on souhaite alors uniquement enlevé la derniere page pour revenir à la précédente
            navigationController?.popViewController(animated: true)
        }
    }


    
    /// Fonction appelée par le bouton "Valider" de l'ecran, permettant de modifier ou créer un paysage
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func doValider(_ sender: Any) {
        
        //On récupère l'epingle positionné par l'utilisateur (peut ne pas exister si l'utilisateur veut utiliser sa position geographique)
        //On filtre les annotations pour ne pas recuperer l'annotation de position de l'utilisateur (qui sera de type MKUserLocation)
        //On veut uniquement une épingle positionné par l'utilisateur.
        let annotation = ui_mapView.annotations.filter({ (annotation) -> Bool in
            annotation is MKPointAnnotation
        }).first
        
        var longitude:Double
        var latitude:Double
        
        //Si une épingle existe, on récupère ses informations
        if annotation != nil {
            latitude = (annotation?.coordinate.latitude)!
            longitude = (annotation?.coordinate.longitude)!
        } else {
            //Sinon, on récupère les informations depuis les coordonnées de la position de l'utilisateur
            longitude = (ui_mapView.userLocation.location?.coordinate.longitude)!
            latitude = (ui_mapView.userLocation.location?.coordinate.latitude)!
        }
        
        //Si paysage en cours est valorisé, c'est qu'on se trouve dans le cas d'une modification
        if let paysageToModify = paysageEnCours {
            //On ouvre une transaction Realm pour modifier l'objet.
            //Si on tente de modifier un objet Realm en dehors, une exception se produira.
            try! realm.write {
                paysageToModify.nom = ui_textField_nom.text!
                paysageToModify.photo = ui_imageView.image?.toData()
                paysageToModify.note = Int(ui_cosmosView.rating)
                paysageToModify.longitude = longitude
                paysageToModify.latitude = latitude
            }
        } else {
            //Sinon il s'agit d'un ajout
            paysageEnCours = paysageManager.enregistrerNouveauPaysage(nom: ui_textField_nom.text!, photo: ui_imageView.image?.toData(), note: Int(ui_cosmosView.rating), latitude: latitude, longitude: longitude)
        }
        
        //On passe par cette transition pour effectuer du traitement dans le TableViewController (mettre à jour la liste)
        performSegue(withIdentifier: "retourListePaysage", sender: self)
    }
    
    
    /// Méthode appelée par le GestureRecognizer si il s'agit d'un "Tap"
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func doTapImageView(_ sender: Any) {
        //On instancie un controller du SDK permettant de choisir des images de la bibliotheque
        let imagePicker = UIImagePickerController()
        //On veut piocher dans la bibliotheque. On pourrait selectionner ".camera" pour prendre une photo
        imagePicker.sourceType = .photoLibrary
        //On précise que le delegate du picker est cette classe meme.
        //Le picker declenchera l'appel a des methodes de son delegate lorsqu'on annulera ou choisira une image
        imagePicker.delegate = self
        //Pour terminer, on présente le controller à l'ecran.
        present(imagePicker, animated: true)
    }
    
    
    /// Methode appelée par le picker d'image lors du clic sur "Annuler"
    ///
    /// - Parameter picker: <#picker description#>
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        //On enleve simplement le picker dans ce cas là
        dismiss(animated: true)
    }
    
    
    /// Methode appelée par le pocker lorsqu'on a choisi une image
    ///
    /// - Parameters:
    ///   - picker: le picker appelant
    ///   - info: un dictionnaire de meta données
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        //L'image est récupéré dans le dictionnaire avec la clé décrite par la constante UIImagePickerControllerOriginalImage
        //Ce dictionnaire peut aussi contenir l'image modifiée par l'utilisateur ainsi que d'autres données
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        //On met à jour l'imageView avec la nouvelle image
        ui_imageView.image = image
        //On enleve le picker de l'ecran
        dismiss(animated: true)
    }
    
    
    /// Méthode appelée par le composant MapView à chaque fois que la position de l'utilisateur est mise à jour
    ///
    /// - Parameters:
    ///   - mapView: <#mapView description#>
    ///   - userLocation: <#userLocation description#>
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        //Creation d'une region pour centrer sur la position de l'utilisateur
        let region = MKCoordinateRegion(center: userLocation.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2))
        ui_mapView.setRegion(region, animated: true)
    }

    
    /// Fonction appelée par le Gesture Recognizer lorsqu'un Long Press est detecté
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func mapViewLongPressed(_ sender: UILongPressGestureRecognizer) {
        //On fait attention à capter l'evenement lors du début de la detection pour eviter de repasser ici lorsque l'evenement touche à sa fin.
        if(sender.state == .began) {
            //On enlève toutes les annotations existantes pour que l'utilisateur en puisse placer qu'une seule épingle
            ui_mapView.removeAnnotations(ui_mapView.annotations)
            
            //On récupère les coordonnées X,Y dans le composant MapView
            let touchPoint = sender.location(in: ui_mapView)
            //On les converti en Longitude/LAtitude grace à une méthode exposée par le composant
            let coordTouched = ui_mapView.convert(touchPoint, toCoordinateFrom: ui_mapView)
            //On créé ensuite un objet Annotation qu'on ajoute à la liste des annotations du composant
            let annotation = MKPointAnnotation()
            annotation.coordinate = coordTouched
            ui_mapView.addAnnotation(annotation)
        }
    }
    
    
    /// Méthode appelée par la MapView pour savoir comment afficher ses annotations
    ///
    /// - Parameters:
    ///   - mapView: <#mapView description#>
    ///   - annotation: <#annotation description#>
    /// - Returns: <#return value description#>
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        //On vérifie que nous ne sommes pas entrain de traiter l'annotation de type 'Position géographique de l'utilsiateur'
        if !(annotation is MKUserLocation) {
            //A partir d'une annotation, on créé une PinAnnotationView (Epingle rouge)
            let annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "pin-annotation")
            //On précise qu'on souhaite une animation
            annotationView.animatesDrop = true
            //Et on retourne l'annotation créée
            return annotationView
        } else {
            return nil
        }
    }
    
    
    /// Méthode appelée par le bouton "Partager"
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func btnShareClicked(_ sender: UIBarButtonItem) {
        //On peut créer un texte et une image
        let texte = "Ce paysage \(paysageEnCours!.nom) etait trop beau !"
        let photo = paysageEnCours?.photo?.toUIImage()
        
        //On ajoute tout cela dans un tableau d'objet
        let objetToShare = [texte, photo!] as [Any]
        
        //On declare un controller du framework appelée UIACtivityViewController en lui donnant le tableau d'objet
        let activityVC = UIActivityViewController(activityItems: objetToShare, applicationActivities: nil)
        
        //On customise la présentation pour le cas d'une utilisation iPad
        activityVC.popoverPresentationController?.barButtonItem = sender
        
        //On présente ce view controller qui nous proposera la liste des applications compatibles
        present(activityVC, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
