//
//  PaysageManager.swift
//  LandscapeTracker
//
//  Created by Xenon on 28/09/2017.
//  Copyright © 2017 ABCDEFAB. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift


/// Le manager qui va gerer les paysages
class PaysageManager {
    
    //Instance statique partagée du manager
    static let shared = PaysageManager()
    
    //Instance partagée de Realm permettant de récuperer et stocker des paysages
    var realm = (UIApplication.shared.delegate as! AppDelegate).realm
    
    
    /// Fonction permettant de charger les paysages
    ///
    /// - Returns: La liste des paysages
    func chargerPaysages() -> [Paysage] {
        //Pour DEBUG, on affiche le path du fichier Realm
        print(realm.configuration.fileURL!)
        
        //Interrogation de Realm pour récuperer l'ensemble des paysages enregistrés
        var listePaysage:[Paysage] = Array(realm.objects(Paysage.self))
        
        //Si Realm ne contient rien, alors on créé des données bidons
        if(listePaysage.isEmpty) {
            
            let paysage1 = Paysage()
            paysage1.nom = "Plaine"
            paysage1.photo = UIImage(named: "paysage1")?.toData()
            paysage1.note = 5
            listePaysage.append(paysage1)
            
            let testPaysage2 = Paysage()
            testPaysage2.nom = "Vallée"
            testPaysage2.photo = #imageLiteral(resourceName: "paysage2").toData()
            testPaysage2.note = 4
            
            listePaysage.append(testPaysage2)
            
            let testPaysage3 = Paysage()
            testPaysage3.nom = "Route"
            testPaysage3.photo = #imageLiteral(resourceName: "paysage3").toData()
            testPaysage3.note = 3
            listePaysage.append(testPaysage3)
        }
        
        return listePaysage
        
    }
    
    
    /// Fonction permettant d'initialiser un nouveau paysage
    ///
    /// - Parameters:
    ///   - nom: le nom
    ///   - photo: la photo
    ///   - note: la note
    /// - Returns: Le paysage créé
    func enregistrerNouveauPaysage(nom:String, photo:Data?, note:Int, latitude:Double, longitude:Double) -> Paysage {
        let newPaysage = Paysage()
        newPaysage.nom = nom
        newPaysage.photo = photo
        newPaysage.note = note
        newPaysage.longitude = longitude
        newPaysage.latitude = latitude
        
        //On ajoute le nouveau paysage à Realm
        try! realm.write {
            realm.add(newPaysage)
        }
        return newPaysage
    }
    
}
