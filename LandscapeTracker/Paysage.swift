//
//  Paysage.swift
//  LandscapeTracker
//
//  Created by Xenon on 28/09/2017.
//  Copyright © 2017 ABCDEFAB. All rights reserved.
//

import Foundation
import RealmSwift


/// Classe décrivant un paysage
class Paysage : Object {
    
    dynamic var nom:String = ""
    dynamic var photo:Data?
    dynamic var note:Int = 0
    dynamic var latitude:Double = 0.0
    dynamic var longitude:Double = 0.0
    
}
