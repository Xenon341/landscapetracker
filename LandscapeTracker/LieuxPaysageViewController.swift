//
//  LieuxPaysageViewController.swift
//  LandscapeTracker
//
//  Created by Xenon on 19/10/2017.
//  Copyright © 2017 ABCDEFAB. All rights reserved.
//

import UIKit
import MapKit


/// Classe utilisée pour piloter la vue affichant l'ensemble des lieux ou les paysages ont été pris en photo
class LieuxPaysageViewController: UIViewController, MKMapViewDelegate {
    
    let paysageManager = PaysageManager.shared
    var listePaysages:[Paysage] = []

    @IBOutlet weak var ui_mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //On précise que ce controller est le delegate de ntore composant MapView
        ui_mapView.delegate = self
        
        listePaysages = paysageManager.chargerPaysages()
        
        for paysage in listePaysages {
            //On vérifie que le paysage possède bien des coordonnées géographiques
            if(paysage.latitude != 0.0 && paysage.longitude != 0.0) {
                //Creation d'un objet annotation pour chaque paysage
                let pin = MKPointAnnotation()
                pin.coordinate = CLLocationCoordinate2D(latitude: paysage.latitude, longitude: paysage.longitude)
                //Ajout d'un nom et d'un sous titre, qu'on affichera dans le "Callout"
                pin.title = paysage.nom
                pin.subtitle = "Note : \(paysage.note)"
                ui_mapView.addAnnotation(pin)
            }
        }
        
        //Creation autoamtique d'une region qui contiendra l'ensemble des epingles.
        //Similaire à map.setRegion mais pour un ensemble de positions à afficher simultanément
        ui_mapView.showAnnotations(ui_mapView.annotations, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /// Fonction appelée pour chaque annotation dans le composant mapView
    ///
    /// - Parameters:
    ///   - mapView: <#mapView description#>
    ///   - annotation: <#annotation description#>
    /// - Returns: <#return value description#>
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let annotation = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "monPin")
        //On veut une animation
        annotation.animatesDrop = true
        //On veut afficher un Callout (cliquer sur l'epingle rouge pour l'afficher)
        annotation.canShowCallout = true
        return annotation
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
