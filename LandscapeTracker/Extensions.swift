//
//  Extensions.swift
//  LandscapeTracker
//
//  Created by Xenon on 28/09/2017.
//  Copyright © 2017 ABCDEFAB. All rights reserved.
//

import Foundation
import UIKit


// MARK: - Extensions du type UIImage
extension UIImage {
    
    
    /// Fonction permettant de convertir un objet UIImage en un objet Data
    ///
    /// - Returns: l'objet Data correspondant à l'UIIMage
    func toData() -> Data? {
        return UIImageJPEGRepresentation(self, 1)
    }
    
}


// MARK: - Extensions du type Data
extension Data {
    
    
    /// Fonction permettant de convertir un objet Date en un objet UIImage
    ///
    /// - Returns: l'objet UIImage correspondant à l'objet Data
    func toUIImage() -> UIImage? {
        return UIImage(data: self)
    }
    
}
