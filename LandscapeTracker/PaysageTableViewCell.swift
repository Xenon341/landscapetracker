//
//  PaysageTableViewCell.swift
//  LandscapeTracker
//
//  Created by Xenon on 28/09/2017.
//  Copyright © 2017 ABCDEFAB. All rights reserved.
//

import UIKit
import Cosmos


/// Classe qui pilote l'affichage des cellule de la tableView
class PaysageTableViewCell: UITableViewCell {

    @IBOutlet weak var ui_imageView: UIImageView!
    @IBOutlet weak var ui_label_nom: UILabel!
    @IBOutlet weak var ui_cosmosView: CosmosView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    /// Fonction qui permet de configurer une cellule selon un Paysage donné
    ///
    /// - Parameter paysage: Le paysage pour lequel on souhaite configurer la cellule
    func configurerCellule(paysage:Paysage) {
        ui_imageView.image = paysage.photo?.toUIImage()
        ui_label_nom.text = paysage.nom
        ui_cosmosView.rating = Double(paysage.note)
    }

}
