//
//  ListePaysageTableViewController.swift
//  LandscapeTracker
//
//  Created by Xenon on 28/09/2017.
//  Copyright © 2017 ABCDEFAB. All rights reserved.
//

import UIKit


/// Classe pilotant la tableView qui liste les paysages
class ListePaysageTableViewController: UITableViewController {
    
    /// Le manager qui va gerer nos paysages
    let paysageManager = PaysageManager.shared
    
    /// La liste qui sera utilisée pour peupler la tableview
    var listePaysage:[Paysage] = []
    
    
    /// Méthode appellée lorsque la vue est chargée (avant son apparition
    /// Configuration de la vue ici
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //On charge la liste des paysages depuis le manager
        listePaysage = paysageManager.chargerPaysages()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    
    /// Méthode appelée par la tableview pour connaitre le nombre de section à afficher
    ///
    /// - Parameter tableView: la tableview appelante
    /// - Returns: Le nombre de section à afficher
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    
    /// Méthode appelée par la tableview pour connaitre le nombre de ligne d'une section donnée
    ///
    /// - Parameters:
    ///   - tableView: la tableview appelante
    ///   - section: la section concernée
    /// - Returns: le nombre de ligne pour la section donnée
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //Ici, on retourne le nombre d'element de notre liste de paysage
        return listePaysage.count
    }
    
    
    /// Méthode appellée par la tableview pour obtenir une cellule pour une section/ligne (IndexPath) donnée
    ///
    /// - Parameters:
    ///   - tableView: la tableview appelante
    ///   - indexPath: L'indexpath (couple section/ligne) pour lequel la tableview veut une cellule
    /// - Returns: La cellule configurée
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //On ré utilise une cellule existante pour des raisons de performance
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellPaysage", for: indexPath)
        
        //On vérifie si la cellule en question est bien une PaysageTableViewCell
        if let cellTypee = cell as? PaysageTableViewCell {
            
            //Si oui, on récupère le paysage correspondant à la ligne en cours
            let paysage = listePaysage[indexPath.row]
            //On configure la cellule pour afficher les informations du paysage
            cellTypee.configurerCellule(paysage: paysage)
            //On retourne la cellule
            return cellTypee
        } else {
            //Cas qui ne devrait jamais arriver
            return cell
        }
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        //On prepare la transition vers la page de détail/modification
        if segue.identifier == "detailPaysage" {
            //On verifie qu'on arrive bien à récuperer la cellule courante ainsi que l'indexPath courant et le ViewController de destination
            if let cellule = sender as? PaysageTableViewCell, let indexPath = tableView.indexPath(for: cellule), let destinationVC = segue.destination as? PaysageViewController {
                //Si oui, on recupere le paysage courant et on le donne au ViewController de detail/modification
                let paysageEnCours = listePaysage[(indexPath.row)]
                destinationVC.paysageEnCours = paysageEnCours
            }
        }
    }
    
    
    /// Fonction appellée lors d'un "Rewind" depuis l'ecran Ajout/Detail lors d'une execution par code de la transition
    ///
    /// - Parameter segue: Transition de rembobinage
    @IBAction
    func rewindToListe(_ segue: UIStoryboardSegue) {
        if let indexPathCourant = tableView.indexPathForSelectedRow, let sourceVC = segue.source as? PaysageViewController {
            //Cas retour de modification car une cellule est selectionnée
            listePaysage[indexPathCourant.row] = sourceVC.paysageEnCours!
            //On recharge uniquement la ligne modifiée
            tableView.reloadRows(at: [indexPathCourant], with: .automatic)
        } else if let sourceVC = segue.source as? PaysageViewController {
            //Mode creation
            let newPaysage = sourceVC.paysageEnCours
            
            //Creation d'un IndexPath pour ajouter la nouvelle valeur a la liste
            let newIndexPath = IndexPath(row: listePaysage.count, section: 0)
            listePaysage.append(newPaysage!)
            //On créé une nouvelle ligne dans la tableView
            tableView.insertRows(at: [newIndexPath], with: .automatic)
        }
    }
}
