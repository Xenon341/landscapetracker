# README #

Comment faire fonctionner le projet

### What is this repository for? ###

* LandscapeTracker
* V1.0

### How do I get set up? ###

* Récuperer les sources de la branche qui vous interesse
* Installer Cocoapods (sudo gem install cocoapods)
* Utiliser Cocoapods et faire un "pod install" pour récuperer les dependances

### Swift 4 ###
* Une branche Swift 4 est disponible pour les gens sur Xcode 9
